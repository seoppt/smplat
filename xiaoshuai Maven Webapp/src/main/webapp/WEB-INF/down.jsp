<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>武威APP下载页面</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<script type="text/javascript" src="<%=path %>/js/jquery-1.10.2.js"></script>
	<!-- 下载页面的CSS -->
	<link rel="stylesheet" type="text/css" href="<%=path %>/css/down/base.css">
	<link rel="stylesheet" type="text/css" href="<%=path %>/css/down/fullPage.css">
	<link rel="stylesheet" type="text/css" href="<%=path %>/css/down/jquery.bxslider.css">
	<link rel="stylesheet" type="text/css" href="<%=path %>/css/down/welcome.css">
	<!-- 下载页面的JS -->
	<script type="text/javascript" src="<%=path %>/js/down/jquery.bdxslider.js"></script>
	<script type="text/javascript" src="<%=path %>/js/down/jquery.fullPage.min.js"></script>
	<script type="text/javascript" src="<%=path %>/js/down/main.js"></script>
	
	<script type="text/javascript">
$(document).ready(function() {
		my_section1.init();
  $('#welcome').fullpage({
	'verticalCentered': false,
	'css3': true,
	'sectionsColor': ['#6cbb52','#e89c38', '#40a3e1'],
	'navigation': true,
	'navigationPosition': 'right',
			//'easing' : 'easeInOutBounce',
			'afterRender' : function (index,anchorLink ) {
				$(".iphone_img").addClass('fadeInTop');
			},
			'afterSlideLoad':function(index ) {

			}
			,
	'afterLoad': function(anchorLink, index){
				if( index == 2) {
					$(".iphone_img").removeClass('fadeInTop');
				}
				if(index==3){
					my_section1.init();
				}
				if(index==1){
					my_section1.init();
				}
				if(index==2){
					my_section2.init();
				}

	},
	'onLeave': function(index, nextIndex, direction){
	  if( nextIndex  == 1 ) {
					$(".iphone_img").addClass('fadeInTop');
				}
				if(index==1){
					my_section1.anim();
				}
				if(index==3){
					my_section1.anim();
				}
				if(index==2){
					my_section2.anim();
				}
	}
  });
});
</script>
  </head>
  
  <body>
    <div id="welcome">
        <div class="section" id="section0">
           <div class="my_section">
                <div class="my_codoon fl">
                    <div class="text1_box">
                        <div class="text1 fl">武威移动APP</div>
                        <div class="blank0"></div>
                    </div>				     
                    <div class="text2"><p>手机实时查看运行情况</p></div>
                    <div class="download_box">
                        <div class="left">
                            <div class="icon erweima">
                            <img class="iphone_xxoo" src="images/down/iphone_xxoo.png">
                            </div>
                        </div>
                        <div class="right">
                            <a href="<%=path %>/anzhuangbao/wuwei.apk" class="item pc_download item_pc"  style="margin-bottom:15px;">Andriod版下载</a>
                            <a href="javascript:void(0);" class="item pc_download item_pc" onclick="alert('iOS客户端即将上线');">iPhone版下载</a>
                            <a href="<%=path %>/anzhuangbao/wuwei.apk" class="item mobile_download mobile_icon"><img src="images/down/now_download.png" alt=""></a>
                            <div id="noios"></div>
                        </div>
                        <div class="blank10"></div>
                    </div>
                </div>
                <div class="iphone fr">
                    <div class="iphone_icon iphone_img">
                      <div id="iphone_slider_wrapper">
                        <div id="iphone_slider">
                          <ul id="iphone_slider_ul">
                            <li><img src="images/down/phone1.jpg"></li>
                            <li><img src="images/down/phone2.jpg"></li>
                            <li><img src="images/down/phone3.jpg"></li>
                            <li><img src="images/down/phone4.jpg"></li>
                            <li><img src="images/down/phone5.jpg"></li>
                          </ul>
                        </div>
                      </div>
                      <img class="iphone_xxoo" src="images/down/iphone_xxoo.png">
                    </div>
                </div>
                <div class="blank0"></div>
           </div>
        </div>
    </div>
        <div class="fixed head">
        <div class="head_content">
            <div class="icon logo logo_blue fl"></div>
            <div class="fr login_box">
            </div>
        </div>
        <div class="line"></div>
    </div>
  </body>
</html>
