<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<c:set var="ctx" value="${pageContext.request.contextPath}"/>
<!DOCTYPE html>
<html>
<head>
<jsp:include page="/common/meta.jsp" />
<meta http-equiv="X-UA-Compatible" content="edge" />
<title>菜单管理--修改</title>
</head>
<body>
	<form action="">
		<table class="">
			<tr>
				<td>菜单名称:</td>
				<td><input type="text" value="${menus.menuname}">
				</td>
			</tr>
			<tr>
				<td>菜单URL:</td>
				<td><input type="text" value="${menus.url}">
				</td>
			</tr>
			<tr>
				<td>菜单图标:</td>
				<td><input type="text" value="${menus.icon}">
				</td>
			</tr>
		</table>
		<div class="b-button">
			<a href="#">save</a>
			<a href="#">close</a>
		</div>
	</form>
</body>
</html>