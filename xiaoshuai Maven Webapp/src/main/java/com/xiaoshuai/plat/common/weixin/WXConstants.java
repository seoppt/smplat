package com.xiaoshuai.plat.common.weixin;

import com.xiaoshuai.plat.vo.AccessToken;

/**
 * 
 * @author 小帅帅丶
 * @Title WXConstants
 * @时间   2017-1-11上午11:20:12
 */
public class WXConstants {
	public static final String appId="wx2d39c6c31ed5f144";
	public static final String appSecret="a593fa37e25ddc96027462ec2c3fa2ce";
	public static final String TOKEN="xiaoshuai";//token
	public static final String SCOPE = "snsapi_userinfo";
	public static final String EncodingAESKey = "XmYszc7iq8wdrYE7FPLEGCqbT2txAhgdhfNewVOqWUZ";
	public static final String AOUTH_URL="";
	public static  AccessToken ACCESS_TOKEN=null;
	public static final String MCH_ID="您自己的商户号";
	public static AccessToken accessToken = new AccessToken();
	public static AccessToken getACCESS_TOKEN() {
		return ACCESS_TOKEN;
	}
	public static void setACCESS_TOKEN(AccessToken aCCESS_TOKEN) {
		ACCESS_TOKEN = aCCESS_TOKEN;
	}
	public static AccessToken getAccessToken() {
		return accessToken;
	}
	public static void setAccessToken(AccessToken accessToken) {
		WXConstants.accessToken = accessToken;
	}
	public static String getAppid() {
		return appId;
	}
	public static String getAppsecret() {
		return appSecret;
	}
	public static String getToken() {
		return TOKEN;
	}
	public static String getScope() {
		return SCOPE;
	}
	public static String getEncodingaeskey() {
		return EncodingAESKey;
	}
	public static String getAouthUrl() {
		return AOUTH_URL;
	}
	public static String getMchId() {
		return MCH_ID;
	}
	
}
