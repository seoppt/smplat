package com.xiaoshuai.plat.service.impl;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiaoshuai.plat.dao.WXUserInfoDao;
import com.xiaoshuai.plat.model.WXUserInfo;
import com.xiaoshuai.plat.service.WXUserInfoService;
/**
 * 
 * @author 小帅帅丶
 * 实现代码
 * @Title WXUserInfoServiceImpl
 * @时间   2017-1-13下午1:17:38
 */
@Service
public class WXUserInfoServiceImpl implements WXUserInfoService {
	private static final Logger logger = Logger.getLogger(WXUserInfoService.class);
	@Autowired
	private WXUserInfoDao wxUserInfoDao;

	public void save(WXUserInfo entity) throws Exception {
		this.wxUserInfoDao.save(entity);
	}

	public void insert(WXUserInfo entity) throws Exception {
		WXUserInfo wxUserInfo = getByOpenId(entity.getOpenid());
		try {
			if (wxUserInfo != null)
				this.wxUserInfoDao.update(entity);
			else
				this.wxUserInfoDao.insert(entity);
		} catch (Exception e) {
			logger.info("出错了     " + e.getMessage());
		}
	}

	public WXUserInfo getByOpenId(String openid) throws Exception {
		return this.wxUserInfoDao.getByOpenId(openid);
	}

	public void update(WXUserInfo entity) throws Exception {
		this.wxUserInfoDao.update(entity);
	}
}
