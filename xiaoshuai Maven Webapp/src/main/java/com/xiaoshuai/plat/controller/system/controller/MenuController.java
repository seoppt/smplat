package com.xiaoshuai.plat.controller.system.controller;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.ServletRequestUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.xiaoshuai.plat.base.BaseController;
import com.xiaoshuai.plat.controller.system.service.MenuService;
import com.xiaoshuai.plat.pojo.Menu;
import com.xiaoshuai.plat.util.StringUtil;

/**
 * 菜单管理
 * @author 宗潇帅
 * @Title MenuController
 * @时间   2017-2-8下午2:46:30
 */
@Controller
@RequestMapping(value="/menu")
public class MenuController extends BaseController{
	private static Logger logger = Logger.getLogger(MenuController.class);
	@Autowired
	private MenuService menuService;
	/**
	 * 跳转页面
	 * @return
	 */
	@RequestMapping(value="/index")
	public String index(){
		logger.info("点击了菜单管理功能");
		return "/view/system/menu/menu_list";
	}
	/**
	 * datagrid需要展示的列表数据
	 * @return
	 * @throws Exception 
	 */
	@RequestMapping("/list")
	public String list() throws Exception{
		int start = ServletRequestUtils.getIntParameter(request, "page", 1)-1;
		int size = ServletRequestUtils.getIntParameter(request, "rows", 0);
		String name = ServletRequestUtils.getStringParameter(request, "name","");
		String order = StringUtil.getOrderString(request);	//取得排序参数
		logger.info("获取到的参数为  start="+start+" size="+size+" name="+name+" order="+order);
		String result = null;
		try {
			result = menuService.list(name, start, size, order);
			logger.info("查询返回的数据 "+result);
			StringUtil.writeToWeb(result, "html", response);
			return null;
		} catch (Exception e) {
			logger.info("加载菜单管理数据失败了"+e.getMessage());
		}
		StringUtil.writeToWeb(result, "html", response);
		return null;
	}
	/**
	 * 修改方法 
	 * 获取用户选择的数据id，查询是否存在后并返回对象到指定页面
	 * @param id
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/goEdit/{id}")
	public String goEdit(@PathVariable int id) throws Exception{
		try {
			Menu menus = menuService.getMenuById(id);
			request.setAttribute("menus", menus);
		} catch (Exception e) {
			logger.info("获取数据失败"+e.getMessage());
		}
		return "view/system/menu/menu_update";
	}
}
