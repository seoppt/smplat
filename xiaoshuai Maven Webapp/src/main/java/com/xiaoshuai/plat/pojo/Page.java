package com.xiaoshuai.plat.pojo;

import java.io.Serializable;
import java.util.List;
/**
 * 分页类
 * @author 小帅帅丶
 * @Title Page
 * @时间   2017-2-7下午4:08:50
 */
public class Page implements Serializable {
	private static final long serialVersionUID = 1L;
	private List rows;
	private int total;

	public List getRows() {
		return this.rows;
	}

	public void setRows(List rows) {
		this.rows = rows;
	}

	public int getTotal() {
		return this.total;
	}

	public void setTotal(int total) {
		this.total = total;
	}
}
