package com.xiaoshuai.plat.vo.message.req;
/**
 * 音频消息
 * @author 宗潇帅
 * @修改日期 2014-7-11下午6:08:19
 */
public class VoiceMessage extends BaseMessage{
	//媒体ID
	private String mediaId;
	//语音格式
	private String Format;
	public String getMediaId() {
		return mediaId;
	}
	public void setMediaId(String mediaId) {
		this.mediaId = mediaId;
	}
	public String getFormat() {
		return Format;
	}
	public void setFormat(String format) {
		Format = format;
	}
	
}
